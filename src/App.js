import React from 'react';
import CardioPlot from './components/CardioPlot';
import FAQ from './components/FAQ';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import styled from 'styled-components';
import Header from "./components/Header";

const StyledContainer = styled.div`
    margin: 20px;
    display: flex;
    justify-content: center;
`;


class App extends React.Component {
    constructor() {
        super();
        this.state = {
            plotData: [],
            faqs: [],
            tags: [],
        }
    }

    async componentDidMount() {
        const urlEcg = "https://reportservice.mawihealth.com/ecg/";
        const urlFaq = "https://reportservice.mawihealth.com/faq/";
        const ecgResponse = await fetch(urlEcg);
        const plotData = await ecgResponse.json();
        const faqResponse = await fetch(urlFaq);
        const faqs = await faqResponse.json();
        this.setState({plotData: plotData, faqs: faqs, tags: this.getUniqueTags(faqs)});
    }

    getUniqueTags = (faqs) => {
        const allTags = faqs.reduce((acc, curr) => acc.concat(
            curr.tags.map(tag => tag.trim())), []);     // trim() added to fix some inconsistency in received data,
        return [...new Set(allTags)];                   // should be fixed on back-end side
    };

    render() {
        const {plotData, faqs, tags} = this.state;
        return (
            <Router>
                <div>
                    <Header/>
                    <StyledContainer>
                        <Switch>
                            <Route exact path="/plot" render={() => <CardioPlot data={plotData}/>}/>
                            <Route exact path="/faq" render={() => <FAQ faqs={faqs} tags={tags}/>}/>
                            <Redirect to="/plot"/>
                        </Switch>
                    </StyledContainer>
                </div>
            </Router>
        );
    }
}

export default App;
