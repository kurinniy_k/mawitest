function* _uid () {  //simple Unique IDs generator
    let counter = 0;
    while (true) {
        yield 'uid'+counter;
        console.log(counter);
        counter += 1;
    }
}
let uid = _uid();

export {uid};