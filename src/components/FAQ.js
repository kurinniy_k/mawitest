import React, {Component} from 'react';
import styled from 'styled-components';
import SearchBar from "./SearchBar";
import {StyledTag, TagsPanel} from "./TagsPanel";
import {uid} from '../utils/utils';

const StyledNavigation = styled.section`  
    border-left: 1px solid rgba(50, 50, 50, 0.5);
    padding-left: 1em;
    width: 300px;
`;
const StyledFaqs = styled.section`    
    display: flex;   
    justify-content: space-between;
    width: 100%;
`;
const StyledFaqItem = styled.div`
    border-bottom: 1px solid rgba(50, 50, 50, 0.1);
    margin: 0 1em 0px 1em;
    .item-tags {
    display: block;
    div {cursor: default;};
    div + div {margin-left: 0;};
    } 
`;

class FAQ extends Component {
    constructor() {
        super();
        this.state = {
            filteredFaqs: [],
            searchedTags: [],
            searchText: ""
        }
    }

    addSearchedTag = (tag) => {
        this.setState({searchedTags: [...this.state.searchedTags, tag]}, () => this.filterFaqs())
    };

    removeSearchedTag = (removedTag) => {
        this.setState({searchedTags: this.state.searchedTags.filter(tag => tag !== removedTag)}, () => this.filterFaqs())
    };

    toggleSearchedTag = (tag) => {
        const {searchedTags} = this.state;
        if (searchedTags.indexOf(tag) !== -1) this.removeSearchedTag(tag);
        else this.addSearchedTag(tag);
    };

    handleChangeSearch = (event) => {
        this.setState({searchText: event.target.value}, () => this.filterFaqs());
    };

    filterFaqs = () => {
        const {faqs} = this.props;
        const {searchedTags, searchText} = this.state;

        let filteredByTags = faqs.filter(faq => searchedTags.reduce(
            (acc, curr) => acc && (faq.tags.indexOf(curr.trim()) !== -1), true));

        let normalizedSearchText = searchText.toLowerCase().trim();

        let newFilteredFaqs = filteredByTags.filter(faq => faq.text.toLowerCase().includes(normalizedSearchText) ||
            faq.title.toLowerCase().includes(normalizedSearchText));
        this.setState({filteredFaqs: newFilteredFaqs});

    };

    render() {
        const {faqs, tags} = this.props;
        const {searchedTags, searchText, filteredFaqs} = this.state;
        return (
            <StyledFaqs>
                <section>
                    {searchText.length > 0 || searchedTags.length > 0 ?
                        filteredFaqs.map((faq) => <FaqItem key={uid.next().value} faq={faq}/>) :
                        faqs.map((faq) => <FaqItem key={uid.next().value} faq={faq}/>)}
                    {filteredFaqs.length === 0 && (searchText.length > 0 || searchedTags.length > 0) &&
                    <h2>Nothing found</h2>}
                </section>
                <StyledNavigation>
                    <SearchBar handleChange={this.handleChangeSearch} searchText={searchText}/>
                    <TagsPanel tags={tags} searchedTags={searchedTags}
                               toggleSearchedTag={this.toggleSearchedTag}/>
                </StyledNavigation>
            </StyledFaqs>);
    }
}

const FaqItem = ({faq}) => <StyledFaqItem key={uid.next().value}>
    <h3>{faq.title}</h3>
    {faq.text}
    <div className="item-tags">{faq.tags && faq.tags.map((tag) => <StyledTag active={false}
                                                                             key={uid.next().value}>{tag}</StyledTag>)}</div>
</StyledFaqItem>;

export default FAQ;