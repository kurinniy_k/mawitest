import React, {Component} from 'react';
import styled from 'styled-components';

const StyledSearchContainer = styled.div`
    display: flex;
    justify-content: space-between;    
	line-height: 28px;
	input {
	margin-left: 1em;
	}
    `;

class SearchBar extends Component {
    render() {
        const {handleChange, searchText} = this.props;
        return (
            <StyledSearchContainer>
                <b>Search</b>
                <input onChange={handleChange} value={searchText} placeholder="search in FAQs..."/>
            </StyledSearchContainer>);
    }
}

export default SearchBar;