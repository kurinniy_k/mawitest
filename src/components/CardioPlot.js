import React, {Component} from 'react';
import Plot from 'react-plotly.js';

class CardioPlot extends Component {
    render() {
        return (
            <Plot
                data={[
                    {type: 'scatter', x0: 0, y: this.props.data}
                ]}
                layout={{width: 1400, height: 600, title: 'ECG', xaxis: {range: [0, 1200]}, dragmode: 'pan'}}
            />)

    }
}

export default CardioPlot;