import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

const StyledHeader = styled.header`
    height: 38px;
    background-color: rgba(113,133,313);
    padding: 19px 0px 19px 0px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    min-width: 100%;
    a {
    margin: 40px;
    text-decoration: none;
    font-size: 43px;
    color: #cee1ff;
    :active {
    color: white;
    }
    }
`;

class Header extends Component {
    render() {
        return (
            <StyledHeader>
                <Link to="/plot">Plot</Link>
                <Link to="/faq">FAQ</Link>
            </StyledHeader>);
    }
}

export default Header;