import React, {Component} from 'react';
import styled from 'styled-components';
import {uid} from '../utils/utils';

const StyledTags = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const StyledTag = styled.div`
    height: 28px;
	line-height: 28px;
	padding: 0 1em;
	margin: 5px 10px 5px 10px;
	display: inline-block;
	background-color: ${props => props.active ? '#3498db' : 'white'};
	border:  ${props => props.active ? '1px solid transparent' : '1px solid #aaa' };
	border-radius: 3px;
	cursor: pointer;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	color: ${props => props.active ? 'white' : '#333'};
	font-size: 13px;
`;

class TagsPanel extends Component {
    render() {
        const {tags, toggleSearchedTag, searchedTags} = this.props;
        return (
            <section>
                <h2>Tags</h2>
                <StyledTags>
                    {tags.map(tag => <StyledTag key={uid.next().value} active={searchedTags.indexOf(tag) !== -1}
                                                onClick={() => toggleSearchedTag(tag)}>{tag}</StyledTag>)}
                </StyledTags>
            </section>);
    }
}

export {TagsPanel, StyledTag};